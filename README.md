# Mineswipper 

[![Website](https://img.shields.io/badge/Website-Live-brightgreen)](https://minesweeper-by-react.vercel.app/)

Гра __Mineswipper__ відповідає класичним правилам, де ваша мета - відкрити всі безпечні клітини, уникнувши мін. Ця гра була реалізована за допомогою бібліотеки __React__.

Ви можете переглянути деплой мого проекту за посиланням: [Mineswipper](https://minesweeper-by-react.vercel.app/).


## Початок роботи

Щоб розпочати роботу з проектом, слід дотримуватись таких кроків:

1. Клонуйте репозиторій:
```
   git clone https://gitlab.com/sllawwkoo/minesweeper_by_react.git
```
2. Перейдіть до директорії проекту:
```
cd minesweeper_by_react
```

3. Встановіть залежності:
```
npm install
```

4. Запустіть сервер розробки:
```
npm start
```


Додаток буде доступний за адресою http://localhost:3000.

