import style from "./header.module.scss";
import { CountBombs } from "../CountBombs";
import { Button } from "../Button";
import { Timer } from "../Timer";

export function Header({countFlags, firstClick, isVictory, isDefeat}) {

	return(
		<div className={style.container}>
			<div className={style.info}>
				<CountBombs
					countFlags={countFlags}
				/>
				<Button 
				isVictory={isVictory}
				isDefeat={isDefeat}
				/>
				<Timer
					firstClick={firstClick}
					isVictory={isVictory}
				/>
			</div>
		</div>
	)
}