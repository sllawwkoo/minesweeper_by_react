import { motion } from "framer-motion";
import style from './smoke.module.scss';
import { firstImgAnimation, secondImgAnimation } from '../../utils/animation';

export function Smoke() {

	return(
		<div className={style.container}>
			<motion.div 
				className={`${style.image} ${style.firstImg}`}
				variants={firstImgAnimation}
				initial='hidden'
				animate='visible'
			/>
			<motion.div 
				className={`${style.image} ${style.secondImg}`}
				variants={secondImgAnimation}
				initial='hidden'
				animate='visible'
			/>
		</div>
	)
}