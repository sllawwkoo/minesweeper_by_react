import { useState, useEffect, useCallback } from 'react';
import style from './field.module.scss';
import { Cell } from '../Cell';
import { isValidRowAndColumn, bombs, isBomb, getCountBomb, getNeighbors } from '../../utils/helpers/helpers';

export function Field({countFlags, setCountFlags, firstClick, setfirstClick,
	closedCount, setClosedCount, isVictory, setIsVictory, isDefeat, setIsDefeat}) {
	const [bombStates, setBombStates] = useState(new Map(bombs.map(index => [index, false])));
	const [isOpened, setIsOpened] = useState(new Map([...Array(64)].map((_, index) => [index, false])));
	const [emptyState, setEmptyState] = useState(new Map([...Array(64)].map((_, index) => [index, false])));
	const [bombCountAround, setBombCountAround] = useState(new Map([...Array(64)].map((_, index) => [index, null])));
	const [isFlaged, setIsFlaged] = useState(new Map([...Array(64)].map((_, index) => [index, false])));
	const [isCurrentBomb ,setIsCurrentBomb] = useState(new Map([...Array(64)].map((_, index) => [index, false])));
	
	//*** Створюємо поле з комірками***/
	const cells = [...Array(64)];

	console.log(bombs);	//!Delete

	//* Фуункція яка оновлює стан і встановлює на комірку прапорець або прибирає його
	const updateIsFlaged = useCallback((index) => {
		setIsFlaged((prevIsFlaged) => {
			const newIsFlaged = new Map(prevIsFlaged);
			newIsFlaged.set(index, !newIsFlaged.get(index));
			return newIsFlaged;
		});
	}, [setIsFlaged]);

	//* Фуункція яка оновлює стан і вказує яка комірка є поточною бомбою
	const updateIsCurrentBomb = useCallback((index) => {
		setIsCurrentBomb(prevIsCurrentBombState => {
			const newIsCurrentBomb= new Map(prevIsCurrentBombState);
			newIsCurrentBomb.set(index, true); 
			return newIsCurrentBomb;
		});
	}, [setIsCurrentBomb]);
	
	//* Функція яка оновлює стан відкритої комірки і вказує на кількість бомб що оточує її
	const updateBombCountAround = useCallback((index, bombCount) =>  {
		setBombCountAround(prevBombCountMap => {
			const newBombCountMap = new Map(prevBombCountMap);
			newBombCountMap.set(index, bombCount); 
			return newBombCountMap;
		});
	}, [setBombCountAround]);

	//* Фуункція яка оновлює стан і вказує що комірка відкрита
	const updateIsOpened = useCallback((index) => {
		setIsOpened(prevIsOpened => {
			const newIsOpened= new Map(prevIsOpened);
			newIsOpened.set(index, true); 
			return newIsOpened;
		});
	}, [setIsOpened]);

	//* Фуункція яка оновлює стан і вказує що комірка пуста (або немає бомби)
	const updateEmptyState = useCallback((index) => {
		setEmptyState(prevEmptyState => {
			const newEmptyState= new Map(prevEmptyState);
			newEmptyState.set(index, true); 
			return newEmptyState;
		});
	}, [setEmptyState]);

	//*** Функція для відкриття всіх бомб на полі ***/
	function openAllBombs() {
		setBombStates(prevStates => {
			const newState = new Map(prevStates);
			bombs.forEach(index => {
				newState.set(index, true);
				// updateIsOpened(index)
				if(isFlaged.get(index)) {
					setIsFlaged((prevIsFlaged) => {
					const newIsFlaged = new Map(prevIsFlaged);
					newIsFlaged.set(index, false);
					return newIsFlaged;
					});
				}
			});
			return newState;
		});
	};

	 //*** Функція для відкриття порожніх комірок(чи в яких немає бомб) ***/ 
	const openEmptyCell = useCallback((row, column) => {
		const neighbors = getNeighbors(row, column);

		neighbors.forEach(neighbor => {
			const { row: neighborRow, column: neighborColumn } = neighbor;
			const index = neighborRow * 8 + neighborColumn;
			const bombCount = getCountBomb(neighborRow, neighborColumn);

			if (
				isValidRowAndColumn(neighborRow, neighborColumn) && 
				!emptyState.get(index) && 
				!bombStates.get(index)
				) {
					updateBombCountAround(index, bombCount);
					updateIsOpened(index);
					updateEmptyState(index);
				}
		});
	}, [bombStates, emptyState, updateBombCountAround, updateIsOpened, updateEmptyState]);	

	//*** Функція для відкриття комірки ***/
	const openCell = (row, column) => {
		if (!isValidRowAndColumn(row, column)) return;

		const index = row * 8 + column;
		if(isBomb(row, column)) {
			firstClick && setfirstClick(false);
			setIsDefeat(true);
			updateIsCurrentBomb(index)
			openAllBombs()
		};

		!firstClick && (!isBomb(row, column)) && setfirstClick(true);

		const bombCount = getCountBomb(row, column)
		if(bombCount === 0) {
			updateBombCountAround(index, bombCount)
			updateIsOpened(index)
			updateEmptyState(index)
			openEmptyCell(row, column)
		} else {
			updateBombCountAround(index, bombCount)
			updateIsOpened(index)
		}
	}

	//*** Функція як встановлює чи убирає прапорець(клік правої кнопки миші) ***/
	const handleClickContextMenu = (e, index) => {
		e.preventDefault();

		!firstClick && setfirstClick(true);
		
		if (!isOpened.get(index)) {
			updateIsFlaged(index);
			if (!isFlaged.get(index)) {
				setCountFlags((prevCountFlags) => Math.max(prevCountFlags - 1, 0));
			} else {
				setCountFlags((prevCountFlags) => prevCountFlags + 1);
			};
		}
	};

	//*** Функція яка спрацьовує при кліку лівою кнопкою миші ***/
	const handleClickCell = (index) => {
		if (!isFlaged.get(index) && !isOpened.get(index)) {
			const row = Math.floor(index / 8);
			const column = index % 8;
			openCell(row, column);
		}
	};

	//* Викликаємо функцію openEmptyCell для всіх комірок з нульовим значенням bombCountAround
	useEffect(() => {
		bombCountAround.forEach((bombCount, index) => {
			if (bombCount === 0) {
				const row = Math.floor(index / 8);
				const column = index % 8;
				openEmptyCell(row, column);
			}
		});
	}, [bombCountAround, openEmptyCell]);

	//* Відслідковуємо кількість закритих комірок, і якщо їх 10 абе меньше значить перемога
	useEffect(() => {
		const closedCellCount = Array.from(isOpened.values()).filter((isOpen) => !isOpen).length;

		setClosedCount(closedCellCount);

		if(closedCount <= 10){
			setIsVictory(true)
		}
	}, [isOpened, closedCount, setClosedCount, setIsVictory])

	return (
		<div className={style.container}>
			{cells.map((_, index) => {
				return (
					<Cell 
						key={index}
						isCellBomb={bombStates.get(index)}
						isCellEmpty={emptyState.get(index)}
						quantityBombs={bombCountAround.get(index)}
						isFlaged={isFlaged.get(index)}
						handleClickContextMenu={(e) => handleClickContextMenu(e, index)}
						isCurrentBomb={isCurrentBomb.get(index)}
						handleClickCell={() => handleClickCell(index)}
						countFlags={countFlags}
						isVictory={isVictory}
						isDefeat={isDefeat}
					/>
				);
			})}
		</div>
	)
}