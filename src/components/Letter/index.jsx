import { motion } from 'framer-motion';
import { letterAnimation, letterExplode } from '../../utils/animation';

export function Letter({index, letter, isDefeat}) {

	return(
		<motion.div
			variants={letterAnimation(index)}
			initial="hidden"
			animate={isDefeat ? "defeat" : "visible"}
		>
			<motion.span
				variants={letterExplode}
				initial="hidden"
				animate={isDefeat ? "visible" : ''}
			>
				{letter}
			</motion.span>
			
		</motion.div>
	)
}