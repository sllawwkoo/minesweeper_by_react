import style from './cell.module.scss';
import { getClassByCount, getClassName } from '../../utils/helpers/helpers';

export function Cell({ isCellBomb, isCurrentBomb, isCellEmpty, quantityBombs, 
	isFlaged, handleClickContextMenu, handleClickCell, countFlags, isVictory, isDefeat }) {
	const amountBombs = getClassByCount(quantityBombs);

const classNames = {
	[style.closed]: true,
	[style.bombed]: isCurrentBomb,
	[style.bomb]: isCellBomb && !isCurrentBomb,
	[style.flaged]: isFlaged,
	[style.zero]: isCellEmpty && quantityBombs === 0 && !isCellBomb,
	[style[amountBombs]]: !(isCurrentBomb || isCellBomb || isFlaged || (isCellEmpty && quantityBombs === 0 && !isCellBomb)),
};

const cellClassName = getClassName(classNames);

	return (
		<div 
			className={cellClassName}
			onContextMenu={(e) => (countFlags > 0 || isFlaged) && !isVictory && !isDefeat && handleClickContextMenu(e)}
			onClick={() => !isVictory && !isDefeat && handleClickCell()}
		>
		</div>
	)
}