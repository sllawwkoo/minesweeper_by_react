import style from './countBombs.module.scss';

export function CountBombs({countFlags}) {

	return (
		<div className={style.container}>
			<span className={style.amount}>{countFlags.toString().padStart(3, '0')}</span>
		</div>
	)
}