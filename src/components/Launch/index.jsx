import { motion } from 'framer-motion';
import style from './launch.module.scss';

export function Launch({left, bottom, background, randomAnimate}) {

	return(
		<motion.div 
			className={style.container}
			style={{left: left, bottom: bottom}}
			initial={{transform: 'translateY(0vh)'}}
			animate={{
				transform: [`translateY(-5vh)`, `translateY(-110vh)`], 
				transition: {
					delay: `0.${randomAnimate}`,
					duration: `1.5${randomAnimate}`,
					ease: 'linear',
					repeat: Infinity,
				}
			}}
		>
			<motion.div className={style.stem}
				style={{background: background}}
				initial={{opacity: 0}}
				animate={{opacity: 1,
					transition: {
						delay: `0.${randomAnimate}`,
						duration: `5.5${randomAnimate}`,
						ease: 'linear',
						epeat: Infinity,
					}
				}}
			/>
		</motion.div>
	)
}