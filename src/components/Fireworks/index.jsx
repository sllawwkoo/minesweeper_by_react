import { Launch } from "../Launch";
import { getRandomColor } from "../../utils/helpers/helpers";
import style from './fireworks.module.scss';

export function Fireworks() {
	const starts = [...Array(100)];

	return (
		<div className={style.container}>
			{starts.map((_, index) => {
				const randomBottom = Math.floor(Math.random() * 65 + 5);
				const randomColor = getRandomColor();
				const randomAnimate = Math.floor(Math.random() * 98 + 2);

				return(
					< Launch
						key={index}
						left={`${index}%`} 
						bottom={`-${randomBottom}%`} 
						background={`linear-gradient(to bottom, rgba(255, 255, 255, 0), ${randomColor})`}
						randomAnimate={randomAnimate}
					/>
				)
			})
			}
		</div>
	);
}