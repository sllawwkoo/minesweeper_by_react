import style from './title.module.scss';
import { Letter } from '../Letter';

export function Title({isDefeat}) {
	const text = 'Mineswipper';

	return (
		<div className={style.heading}>
			{text.split('').map((letter, index) => (
				<Letter
					key={index}
					letter={letter}
					index={index}
					isDefeat={isDefeat}
				/>
      ))}
		</div>
	)
}