import { getClassName } from "../../utils/helpers/helpers";
import { motion } from "framer-motion";
import { buttonAnimation } from "../../utils/animation";
import style from "./button.module.scss";

export function Button({isVictory, isDefeat}) {
	const reloadPage = () => {
		window.location.reload(); 
	}

const animationVariant = isVictory ? 'victory' : isDefeat ? 'defeat' : 'initial';

	const classNames = {
	[style.start]: true,
	[style.gameOver]: isDefeat,
	[style.win]: isVictory,
};

const buttonClassName = getClassName(classNames)
	
	return (
		<motion.button
			type="button"
			className={buttonClassName}
			onClick={reloadPage}
			initial="initial"
			animate={animationVariant} 
			variants={buttonAnimation}
		/>
	)
}