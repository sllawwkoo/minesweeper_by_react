import { useState } from 'react';
import { Field } from '../Field';
import { Header } from '../Header';
import { Title } from '../Title';
import { Smoke } from '../Smoke';
import { GameOver } from '../GameOver';
import { Win } from '../Win';
import { Fireworks } from '../Fireworks';
import style from './minesweeper.module.scss';

export function Minesweeper() {
	const [closedCount, setClosedCount] = useState(64);
	const [countFlags, setCountFlags] = useState(10);
	const [firstClick, setfirstClick] = useState(false);
	const [isVictory, setIsVictory] = useState(false);
	const [isDefeat, setIsDefeat] = useState(false);

	return (
		<div className={style.wrapper}>
			{isDefeat && <Smoke />}
			{isVictory && <Fireworks />}
			<div className={style.container}>
				{isVictory && <Win />}
				<Title
				isDefeat={isDefeat}
				/>
				<Header 
					countFlags={countFlags}
					firstClick={firstClick}
					isVictory={isVictory}
					isDefeat={isDefeat}
				/>
				<Field 
					countFlags={countFlags}
					setCountFlags={setCountFlags}
					firstClick={firstClick}
					setfirstClick={setfirstClick}
					closedCount={closedCount}
					setClosedCount={setClosedCount}
					isVictory={isVictory}
					setIsVictory={setIsVictory}
					isDefeat={isDefeat}
					setIsDefeat={setIsDefeat}
				/>
				{isDefeat && <GameOver />}
			</div>
		</div>
	)
}