import { useEffect, useRef, useState } from 'react';
import style from './timer.module.scss';

export function Timer({firstClick, isVictory}) {
	const [seconds, setSeconds] = useState(0);
	const timerRef = useRef(null);

	const incrementSeconds =() => {
		setSeconds((prevSeconds) => prevSeconds + 1);
	}

	useEffect(() => {
   if (firstClick && !isVictory) {
      timerRef.current = setInterval(incrementSeconds, 1000);
   } else {
		clearInterval(timerRef.current);
   }
	}, [firstClick, isVictory]);

	const newSeconds = seconds.toString().padStart(3, '0');

	return (
		<div className={style.container}>
			<span className={style.seconds}>{newSeconds}</span>
		</div>
	)
}