import { motion } from "framer-motion";
import style from './gameOver.module.scss';
import { gameOverAnimation, gameMoveAnimation, overMoveAnimation } from "../../utils/animation";

export function GameOver() {

	return(
		<motion.div className={style.container}
			variants={gameOverAnimation}
			initial='start'
			animate='end'		
		>
			<motion.span
			variants={gameMoveAnimation}
			initial='start'
			animate='end'
			>
				GAME
			</motion.span>
			<motion.span
			variants={overMoveAnimation}
			initial='start'
			animate='end'
			>
				OVER
			</motion.span>
		</motion.div>
	)
}