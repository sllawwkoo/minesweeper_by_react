import { motion } from 'framer-motion';
import style from './win.module.scss';
import { winMoveAnimation, youMoveAnimation, youWinAnimation } from '../../utils/animation';

export function Win() {

	return(
		<motion.div className={style.container}
			variants={youWinAnimation}
			initial='start'
			animate='end'		
		>
			<motion.span
			variants={youMoveAnimation}
			initial='start'
			animate='end'
			>
				YOU
			</motion.span>
			<motion.span
			variants={winMoveAnimation}
			initial='start'
			animate='end'
			>
				WIN
			</motion.span>
		</motion.div>
	)

}