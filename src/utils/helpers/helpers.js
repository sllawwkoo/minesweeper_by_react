//*** Генеруємо рандомно 10 бомб ***/
export const bombs = [...Array(64).keys()].sort(() => Math.random() - 0.5).slice(0, 10);

//*** Функція яка перевіряє на валіднасть рядки та стовпчики поля ***/
export function isValidRowAndColumn(row, column) {
	return row >= 0 && row < 8 && column >= 0 && column < 8;
};

//*** Функція яка перевіряє чи є дана комірка бомбою ***/
export function isBomb(row, column, array = bombs) {
	if (!isValidRowAndColumn(row, column)) return false;

	const index = row * 8 + column;

	return array.includes(index);
};

//*** Функція яка перевіряє чи є в сусідніх комірках бомби, повертає кількість бом які є в сусідніх комірках***/
export function getCountBomb(row, column) {
	let count = 0;
	for (let i = -1; i <= 1; i++) {
		for (let j = -1; j <= 1; j++) {
			if (isBomb(row + j, column + i)) {
				count++
			}
		}
	}
	return count;
};

//*** Функція яка встановлює клас відповідно до кількості бомб навколо вибраної комірки ***/
export function getClassByCount(count) {
	switch (count) {
		case 1: return 'one';
		case 2: return 'two';
		case 3: return 'three';
		case 4: return 'four';
		case 5: return 'five';
		case 6: return 'six';
		case 7: return 'seven';
		case 8: return 'eight';
		default: return '';
	}
}

//* Функція яка знаходить всі сусідні комірки до поточної комірки
//* і повертати масив об'єктів з координатами поточної та сусідніх комірок.
export function getNeighbors(row, column) {
	const neighbors = [];
	// Вгору
	neighbors.push({ row: row - 1, column });
	// Вгору Зліва
	neighbors.push({ row: row - 1, column: column - 1 });
	// Вгору Справа
	neighbors.push({ row: row - 1, column: column + 1 });
	// Внизу
	neighbors.push({ row: row + 1, column });
	// Внизу Зліва
	neighbors.push({ row: row + 1, column: column - 1 });
	// Внизу Справа
	neighbors.push({ row: row + 1, column: column + 1 });
	// Зліва
	neighbors.push({ row, column: column - 1 });
	// Справа
	neighbors.push({ row, column: column + 1 });

	return neighbors;
}

//* Функція яка отримує клас в зілежності від стану
export function getClassName(classNames) {
	return Object.entries(classNames)
	.filter(([className, condition]) => condition)
	.map(([className]) => className)
	.join(' ');
}

//* Функція яка рандомно генерує колір
export function getRandomColor() {
	const letters = '0123456789ABCDEF';
	let color = '#';
	for (let i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}